﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PH_MultiAxesGUI.Axes Data get File.vi" Type="VI" URL="../PH_MultiAxesGUI.Axes Data get File.vi"/>
		<Item Name="PH_MultiAxesGUI.Axes Data read File.vi" Type="VI" URL="../PH_MultiAxesGUI.Axes Data read File.vi"/>
		<Item Name="PH_MultiAxesGUI.Axes Data save File.vi" Type="VI" URL="../PH_MultiAxesGUI.Axes Data save File.vi"/>
		<Item Name="PH_MultiAxesGUI.Axes Data.ctl" Type="VI" URL="../PH_MultiAxesGUI.Axes Data.ctl"/>
		<Item Name="PH_MultiAxesGUI.Axis Data.ctl" Type="VI" URL="../PH_MultiAxesGUI.Axis Data.ctl"/>
		<Item Name="PH_MultiAxesGUI.Axis.ctl" Type="VI" URL="../PH_MultiAxesGUI.Axis.ctl"/>
		<Item Name="PH_MultiAxesGUI.calc BG Color Array.vi" Type="VI" URL="../PH_MultiAxesGUI.calc BG Color Array.vi"/>
		<Item Name="PH_MultiAxesGUI.i attribute.ctl" Type="VI" URL="../PH_MultiAxesGUI.i attribute.ctl"/>
		<Item Name="PH_MultiAxesGUI.i attribute.vi" Type="VI" URL="../PH_MultiAxesGUI.i attribute.vi"/>
		<Item Name="PH_MultiAxesGUI.indicate Linkage.vi" Type="VI" URL="../PH_MultiAxesGUI.indicate Linkage.vi"/>
		<Item Name="PH_MultiAxesGUI.InitFrontPanel.vi" Type="VI" URL="../PH_MultiAxesGUI.InitFrontPanel.vi"/>
		<Item Name="PH_MultiAxesGUI.ProcEvents.vi" Type="VI" URL="../PH_MultiAxesGUI.ProcEvents.vi"/>
		<Item Name="PH_MultiAxesGUI.read Database.vi" Type="VI" URL="../PH_MultiAxesGUI.read Database.vi"/>
		<Item Name="PH_MultiAxesGUI.set Axes Setings to GUI.vi" Type="VI" URL="../PH_MultiAxesGUI.set Axes Setings to GUI.vi"/>
		<Item Name="PH_MultiAxesGUI.set Axis Ctrls enabled.vi" Type="VI" URL="../PH_MultiAxesGUI.set Axis Ctrls enabled.vi"/>
		<Item Name="PH_MultiAxesGUI.set Axis Information to GUI Element.vi" Type="VI" URL="../PH_MultiAxesGUI.set Axis Information to GUI Element.vi"/>
		<Item Name="PH_MultiAxesGUI.set Axis Parameters to GUI Elements.vi" Type="VI" URL="../PH_MultiAxesGUI.set Axis Parameters to GUI Elements.vi"/>
		<Item Name="PH_MultiAxesGUI.set Axis Setings to GUI Element.vi" Type="VI" URL="../PH_MultiAxesGUI.set Axis Setings to GUI Element.vi"/>
		<Item Name="PH_MultiAxesGUI.set BG Color to GUI.vi" Type="VI" URL="../PH_MultiAxesGUI.set BG Color to GUI.vi"/>
		<Item Name="PH_MultiAxesGUI.set i Attr.vi" Type="VI" URL="../PH_MultiAxesGUI.set i Attr.vi"/>
		<Item Name="PH_MultiAxesGUI.set Target Position to GUI Element.vi" Type="VI" URL="../PH_MultiAxesGUI.set Target Position to GUI Element.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="PH_MultiAxesGUI.get i attribute.vi" Type="VI" URL="../PH_MultiAxesGUI.get i attribute.vi"/>
		<Item Name="PH_MultiAxesGUI.ProcCases.vi" Type="VI" URL="../PH_MultiAxesGUI.ProcCases.vi"/>
		<Item Name="PH_MultiAxesGUI.ProcPeriodic.vi" Type="VI" URL="../PH_MultiAxesGUI.ProcPeriodic.vi"/>
		<Item Name="PH_MultiAxesGUI.set GUI Refs.vi" Type="VI" URL="../PH_MultiAxesGUI.set GUI Refs.vi"/>
		<Item Name="PH_MultiAxesGUI.set i attribute.vi" Type="VI" URL="../PH_MultiAxesGUI.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PH_MultiAxesGUI.constructor.vi" Type="VI" URL="../PH_MultiAxesGUI.constructor.vi"/>
		<Item Name="PH_MultiAxesGUI.destructor.vi" Type="VI" URL="../PH_MultiAxesGUI.destructor.vi"/>
		<Item Name="PH_MultiAxesGUI.get data to modify.vi" Type="VI" URL="../PH_MultiAxesGUI.get data to modify.vi"/>
		<Item Name="PH_MultiAxesGUI.panel.vi" Type="VI" URL="../PH_MultiAxesGUI.panel.vi"/>
		<Item Name="PH_MultiAxesGUI.set modified data.vi" Type="VI" URL="../PH_MultiAxesGUI.set modified data.vi"/>
	</Item>
	<Item Name="PH_MultiAxesGUI.contents.vi" Type="VI" URL="../PH_MultiAxesGUI.contents.vi"/>
</Library>
